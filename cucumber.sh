#!/bin/bash

node_modules/.bin/cucumber.js -f json test/features/*.feature --require test/features/step_definitions/api_steps.js > test/reports/cucumber.json
cat test/reports/cucumber.json
