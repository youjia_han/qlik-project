/**
 *@youjia han
 * main router for the server
 */
var express = require("express");
var mongojs = require("mongojs");
var bodyParser = require("body-parser");
var wu= require("./utils/webutils.js");
var validate = require("./utils/validation.js");
var db = mongojs('messagelist',['messagelist']);
var app = express();

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.get(/^\/list_[^\/\s]+$/, function(req,res){
	var path = req.url.substring(1,req.url.length)
	var index = path.indexOf('_')
	req.params.datatype = path.substring(0,index);
	req.params.listtype  = path.substring(index+1,path.length);
	wu.chain(req,res,"read",validate.noop);	
});

app.get(/^\/((?!list)[^\/\s]+)(\/(.+))+$/, function(req,res){
 	 req.params.datatype = req.params[0];
	 wu.chain(req,res,"read",validate.noop);		
});

app.post(/^\/((?!list)[^\/\s]+)$/, function(req,res){
	 req.params.datatype = req.params[0];
	 wu.chain(req,res,"create",validate.noop);		
});

app.delete(/^\/((?!list)[^\/\s]+)(\/(.+))+$/,function(req,res){
	 req.params.datatype = req.params[0];
	  wu.chain(req,res,"delete",validate.noop);	
});

app.listen(3000);

function slugs_from_url (url) {
	var endIdx = url.indexOf('?') !== -1 ? url.indexOf('?') : url.length
	return url.substring (1, endIdx )
}
