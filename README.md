# **README** #

## **Quick summary** ##
This App includes functions of add,read and remove. The app also provides determining a message if it is a palindrome.

## **Version** ##
1.0.0
## **Developing Environment** ##
NodeJS+Express+AngularJS+MongoDB
## **Dependencies** ##
{
  "name": "Package.json",
  "version": "1.0.0",
  "description": "Qlik Cloud Team Audition Project",
  "main": "app.js",
  "directories": {
    "test": "test"
  },
  "dependencies": {
    "cucumber": "^0.4.9",
    "express": "^4.12.3",
    "body-parser": "^1.12.4",
    "mongojs": "^0.18.2",
    "request": "^2.55.0",
    "q": "^1.4.1"
  },
  "devDependencies": {},
  "scripts": {
    "test": "bash cucumber.sh"
  },
  "author": "youjia han",
  "license": "ISC"
}

## **Start** ##
node app.js

##**How to run tests**##
bash cucumber.sh

##**Test result format**##
[
  {
    "id": "Message-Features",
    "name": "Message Features",
    "description": "  As a user of the API\n  I want to be able to create, read and delete messages",
    "line": 1,
    "keyword": "Feature",
    "uri": "/Users/youjiahan/Documents/workspace/Mean/test/features/message.feature",
    "elements": [
      {
        "name": "Creating message",
        "id": "Message-Features;creating-message",
        "line": 6,
        "keyword": "Scenario",
        "description": "",
        "type": "scenario",
        "steps": [
          {
            "name": "I am a public user",
            "line": 7,
            "keyword": "Given ",
            "result": {
              "duration": 350611,
              "status": "passed"
            },
            "match": {}
          },
          {
            "name": "I fill the message with content \"{\"content\":\"test123ffff\"}\"",
            "line": 8,
            "keyword": "And ",
            "result": {
              "duration": 113628,
              "status": "passed"
            },
            "match": {}
          },
          {
            "name": "I \"post\" the \"message\" to the server",
            "line": 9,
            "keyword": "When ",
            "result": {
              "duration": 91539,
              "status": "passed"
            },
            "match": {}
          },
          {
            "name": "I should get a status code \"201\" and a response body \"^.*_id\\\":\\\".*\\\".*$\"",
            "line": 10,
            "keyword": "Then ",
            "result": {
              "duration": 23646567,
              "status": "passed"
            },
            "match": {}
          }
        ]
      },...]
## **Sequence diagram** ##
![Screen Shot 2015-05-20 at 11.35.56 PM.png](https://bitbucket.org/repo/r5XBa8/images/1279626429-Screen%20Shot%202015-05-20%20at%2011.35.56%20PM.png)
##**REST API documentation**##

### Message ###

* ### CREATE ###
* POST Method
* Path: /message
* Required header: Content-type: application/json
* Request body format:{"content":"test"}
* Response body format:
{
    "content": "test",
    "_id": "555d5c3170c637217f15e2eb"
}

* ### READ ###
* GET Method
* Path: /message/<message_id>
* Required header: Content-type: application/json
* Response body format:
{
    "_id": "555d5c3170c637217f15e2eb",
    "content": "test"
}

* ### DELETE ###
* DELETE Method
* Path: /message/<message_id>
* Required header: Content-type: application/json
* Response body format:
{
    "res": "success"
}

* ### READ ALL ###
* GET Method
* Path: /list_message
* Required header: Content-type: application/json
* Response body format:
[
    {
        "_id": "555c0cdbd15bc0fc642aefff",
        "content": "test123ffff"
    },
    {
        "_id": "555c0ee1d15bc0fc642af001",
        "content": "test123ffff"
    },
    {
        "_id": "555d5bd270c637217f15e2ea",
        "content": "dfgdfg"
    }
]

### Palindrome ###

* ### isPalindrome ###
* GET Method
* Path: /palindrome/<any string>
* Required header: Content-type: application/json
* Response body format:
{
    "result": true
}