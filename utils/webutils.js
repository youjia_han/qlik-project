

var WebUtils = function (){};
var q = require('q');
var context = require('../handlers/context.js');
/**
*the standard event chain to handle all requests and responses
*/
WebUtils.prototype.chain = function(req, resp, verb,validator){
	var state = { req : req,
				  resp: resp,
				  verb: verb,
				  validator: validator,
				  ctx: {}}
	state.ctx.datatype = req.params.datatype
	//check input parameters
	q.fcall(function(p_state){
		var err = p_state.validator(req.params) 
		if (err){
			err.code = 403
			throw err

		} else
			return p_state 
	},state)
	.then(context.setHandlers)
	.then(function(p_state){
		return p_state.ctx.validator(p_state)
	})
	// persist or retrieve
	.then(function(p_state){
		return p_state.ctx.persistence(p_state)
	})
	// respond with success
	.then(function(p_state){
		p_state.ctx.replySuccess (p_state); 
		return p_state
	})
	.catch (function (err) { 
		// was it a "normal" error, or an unexpected exception?
			resp.status(500).send({
                "status": "server_error.internal_server_error",
                "code": 500,
                "message": "Sever error"
            }) 
	})
	.finally (function () {  }) 
	.done()
}


WebUtils.prototype.createError = function(err){
	var defer = q.defer();
		defer.reject(err);
	return defer.promise;
}


module.exports = new WebUtils();
