/**
* a handler to signify success upon creation
*/
var q = require('q')
module.exports.replySuccessRead = function (state) {
	state.resp.status(200).json(state.resource)
}

module.exports.replySuccessCreate = function (state) {
	state.resp.status(201).json(state.resource)
}

module.exports.replySuccessDelete = function (state) {
	state.resp.status(200).json({res:'success'})
}

module.exports.promiseToReturn = function (value) {
	var defer = q.defer()
	defer.resolve(value)
	return defer.promise
}