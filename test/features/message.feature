Feature: Message Features
    As a user of the API
    I want to be able to create, read and delete messages
    

Scenario: Creating message
	Given I am a public user
  	And I fill the message with content "{"content":"test123ffff"}"
	When I "post" the "message" to the server
	Then I should get a status code "201" and a response body "^.*_id\":\".*\".*$"


Scenario: Reading message
  Given I am a public user
  When I "get" the "message" from the server with id "555c0bb2d15bc0fc642aeffc" 
  Then I should get a status code "200" and a response body "^.*_id\":\".*\".*$"

Scenario: Reading all message
  Given I am a public user
  When I "get" the "list_message" from the server
  Then I should get a status code "200" and a response body "^.*_id\":\".*\".*$"

Scenario: delete message
  Given I am a public user
  When I "delete" the "message" from the server with id "555c0bb2d15bc0fc642aeffc"
  Then I should get a status code "200" and a response body "^.*res\":\"success\".*$"