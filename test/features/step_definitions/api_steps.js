
var validators = require('../support/validators.js').validators
var config = require('../support/config.js')

var stepDefs = function (){
	var context = {}
	this.World = require('../support/world.js').World
	this.Given(/^I am a public user$/, function (callback){
		callback()
	})
	// GIVENs describe permissions or content
	// WHENs describe verbs on nouns
	// THENs describe the desired outcomes
	this.Given(/^I fill the message with content "(.+)"$/,function(content,callback){
		context.content = content
		callback()
	})

	this.When(/^I "(.+)" the "(.+)" from the server with id "(.+)"$/,function(action,datatype,id,callback){
		dataItemWhen (action, datatype, config.url.base+datatype+"/"+id, validators.message.read, callback)
	})

	this.When(/^I "(.+)" the "(.+)" to the server$/,function(action,datatype,callback){
		dataItemWhen (action, datatype, config.url.base+datatype, validators.message.create, callback)
	})

	this.When(/^I "(.+)" the "(.+)" from the server$/,function(action,datatype,callback){
		dataItemWhen (action, datatype, config.url.base+datatype, validators.message.read, callback)
	})

	this.Then(/^I should get a status code "(.+)" and a response body "(.+)"$/,function(status_code,body_regex,callback){
		var expected_outcome =[parseInt(status_code),body_regex]
		sendReq (context.url,
						 context.action,
						 context.content,
						 expected_outcome,
						 context.validator,
						 this.req,
						 callback)
	})

	// setting data up for When during any other method
	function dataItemWhen (action, datatype, url, validator, callback){
		context.url = url
		context.validator = validator
		context.action = action
		callback()
	}

	// uses the setup to send a request to the server, and then validate the outcome
	function sendReq (url, action,form,expected_outcome, validator, request_func, callback){
		request_func (
			url,
			action,
			form,
			function (err){
				callback.fail (new Error(err))
			},
			function (status_code, body) {
				//console.log(body)
				validator (callback, status_code, body, expected_outcome)
			})
	}
}

module.exports = stepDefs