/**
 * VALIDATORS - Accept the callback from Then, the response body and the expected outcome as the arguments.
 * Call callback() on success, and callback.fail(error) on failure
 */

module.exports = new Validators()
function Validators(){}

var messageValidator = function (callback, status_code, response_body, expected_outcome) {
	if(expected_outcome[1])
		var regex = new RegExp(expected_outcome[1])
	if(status_code!=expected_outcome[0]){
		console.log('>>> FAIL >>>  STATUS: ' + status_code + ' RESPONSE: ' + response_body + ' EXPECTED: ' + JSON.stringify (expected_outcome))
		callback.fail (new Error("status_code does not match the expected one"))
	}
	else if(!!regex && !regex.test(response_body)){
		console.log('>>> FAIL >>>  STATUS: ' + status_code + ' RESPONSE: ' + response_body + ' EXPECTED: ' + JSON.stringify (expected_outcome))
		callback.fail (new Error("response_body does not match the expected regular expression"))
	}
	else {
		callback()
	}

}

Validators.prototype.validators = {
	message: {
		create: messageValidator, // TODO
		read:   messageValidator, // TODO
		delete: messageValidator,  // TODO
	}
}