var request = require('request')
var db = require('./db')
var World = function World(done) {

	// url: absolute url to the endpoint
	// method: GET, POST, PUT, DELETE, HEAD etc..
	// form: the object containing the form key-value pairs
	// onErr: function (err_str). will be called with an error if the result won't go as expected.
	// onSuccess: function(). will be called on success. 
	//
	this.req = function (url, method,form, onErr, onSuccess){
		options = {}
		options.uri = url
		options.method = method
		options.body = form
		console.log(url)
		console.log(method)
		console.log(form)
		options.headers = { 'Content-Type':'application/json'}
      	request (options, function (err, resp, body){
			console.log('resp'+JSON.stringify(body))
			if (err) {
				onErr (err)
			} else
				onSuccess(resp.statusCode, body)
		})
	}

	this.dbReset = function(callback) {
		db.dbReset(callback)
	}

	done()
}
exports.World = World