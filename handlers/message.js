/**
 *Handlers for any type of item that has a corresponding ./model/
 *
 */
var q = require('q')
var message = require('../model/message.js')
var handler_utils = require('../utils/handlerutils.js')

module.exports.create = function(state){
	state.ctx.validator = handler_utils.promiseToReturn
	state.ctx.persistence = persistence_create
	state.ctx.replySuccess = handler_utils.replySuccessCreate
}

module.exports.read = function(state){
	state.ctx.validator = handler_utils.promiseToReturn
	state.ctx.persistence = persistence_read
	state.ctx.replySuccess = handler_utils.replySuccessRead
}

module.exports.delete = function(state){
	state.ctx.validator = handler_utils.promiseToReturn
	state.ctx.persistence = persistence_delete
	state.ctx.replySuccess = handler_utils.replySuccessDelete
}

module.exports.readAll = function(state){
	state.ctx.validator = handler_utils.promiseToReturn
	state.ctx.persistence = persistence_readAll
	state.ctx.replySuccess = handler_utils.replySuccessRead
}

/**
 * generic handler for datatype reads
 */
function persistence_read(state){
	var id  = state.req.params[2]
	var collection = state.ctx.datatype
	return message.read(id,collection).then(function(res){
		state.resource = res
		return state
	})
}
/**
 * generic handler for datatype creates
 */
function persistence_create(state){
	var content = state.req.body;
	var collection = state.ctx.datatype
	return message.create(content,collection).then(function(res){
		state.resource = res
		return state
	})
}
/**
 * generic handler for datatype deletes
 */
function persistence_delete(state){
var id  = state.req.params[2]
	var collection = state.ctx.datatype
	return message.delete(id,collection).then(function(res){
		state.resource = res
		return state
	})
}
/**
 * generic handler for datatype readAll
 */
function persistence_readAll(state){
	var collection = state.req.params.listtype;
	return message.readAll(collection).then(function(res){
		state.resource = res
		return state
	})
}




