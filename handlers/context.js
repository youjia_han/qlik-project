/**
 * Provides functions that set the following functions in the state's ctx attribute:
 *  ==> All functions return a promise with 'state', except for replySuccess
 *  ==> All functions comply with the rule: on failure, throw "status_code-->message"
 * - state.ctx.persistence (state)
 *		performs the verb on the resource in persistence
 *
 */
module.exports = new Context();
var item = require('./message.js')
var palindrome = require('./palindrome.js')
var wu = require('../utils/webutils.js')
function Context(){

}
/**
 * a routing to function-setting-functions depending on the type and verb
 */
var object_function_map = {
	'message':{
		'create' : item.create,
		'read' : item.read,
		'delete' : item.delete
	},
	'palindrome':{
		'read': palindrome.check
	}
}
var list_function_map ={
	'list':{
	'message':{
		'read' : item.readAll
	}
	}
}

/**
 * assign functions by type (message/list), datatype and verb
 */
Context.prototype.setHandlers = function(state){
	var verb = state.verb;
	var datatype = state.ctx.datatype
	if(datatype=='list'){
	var listType = state.req.params.listtype
	var listhandler = list_function_map[datatype][listType];
	if(listhandler){
		listhandler[verb](state);
	}
	else{
		throw wu.createError('invalid_resource_type');
	}
	}
	else{
	var handler = object_function_map [datatype]
	if(handler){
		handler[verb](state);
	}else{
		throw wu.createError('invalid_resource_type');
	}
	}
	return state
}