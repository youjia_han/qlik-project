/**
 *Handlers for any type of item that has a corresponding ./model/
 *
 */
var q = require('q')
var handler_utils = require('../utils/handlerutils.js')

module.exports.check = function(state){
    state.ctx.validator = persistence_check
    state.ctx.persistence = handler_utils.promiseToReturn
    state.ctx.replySuccess = handler_utils.replySuccessRead
}

/**
 * generic handler for palindrome check
 * result =={"result": true}
 */
function persistence_check(state){
    var message  = state.req.params[2]
    var d = q.defer()
    var result = true;
    for (i=0;i<=Math.floor(message.length/2);i++){
        if(message.charAt(i)!=message.charAt(message.length-1-i))
            result = false;
    }
    state.resource={"result":result}
    d.resolve(state)
    return d.promise

}