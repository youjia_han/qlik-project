var myApp = angular.module('myApp',[]);

myApp.controller('AppCtrl',['$scope','$http',
	function($scope,$http){
var refresh = function(){
		$http.get('/list_message').success(function(res){
			$scope.messagelist = res;
			$scope.message='';
		});
	}
refresh();
	$scope.addMessage = function(){
		var body = $scope.message.content;
		var content = {"content":body}
		$http.post('/message',content).success(function(res){
			refresh();
		});
	};
 
	$scope.remove = function(id){
		$http.delete('/message/'+id).success(function(res){
			refresh();
		});
	}

	$scope.retrieve = function(id){
		$http.get('/message/'+id).success(function(res){
			$scope.message = res
		})
	}

	$scope.isPalindrome = function(){
		var message = $scope.message.content;
		$http.get('/palindrome/'+message).success(function(res){
			if(res.result) alert("This message is palindrome")
			else alert("This message is not palindrome")

		})
	}

	$scope.clear = function(){
		$scope.message='';
	}

	$scope.messageValidation = function(method){
		var body = $scope.message.content;
		body==null ? alert("Please enter something!"):method();
	}

}]);