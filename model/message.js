/**
 * Functions that operate on item objects and interface with persistence
 *
 */
var q = require('q');
var mongojs = require("mongojs");
var db_name = 'project';
/**
 *
 * @param id: item_id
 * @param collection: which collection in the db
 * @returns {
    "_id": "555c0bf0d15bc0fc642aeffd",
    "content": "test123ffff"
}
 */
module.exports.read = function (id,collection){
	var d = q.defer()
	var db = mongojs(db_name,[collection]);
		db[collection].findOne({_id: mongojs.ObjectId(id)},function(err,doc){
			if(err) d.reject(err)
			else d.resolve(doc)		
		});
	return d.promise	
}

/**
 *
 * @param content: {"content":"test123"}
 * @param collection: which collection in the db
 * @returns {
    "content": "test123",
    "_id": "555d35da1d0a645975c5a73d"
}
 */
module.exports.create = function(content,collection){
	var d = q.defer()
	var db = mongojs(db_name,[collection]);
	console.log(content);
		db[collection].insert(content,function(err, doc){
			if(err) d.reject(err)
			else d.resolve(doc)		
		});
	return d.promise
}
/**
 *
 * @param id: item_id
 * @param collection: which collection in the db
 * @returns {
    "res": "success"
}
 */
module.exports.delete = function(id,collection){
	var d = q.defer()
	var db = mongojs(db_name,[collection]);
	db[collection].remove({_id: mongojs.ObjectId(id)},function(err,doc){
			if(err) d.reject(err)
			else d.resolve(doc)		
		});
	return d.promise
}
/**
 *
 * @param collection: collection: which collection in the db
 * @returns [
 {
    "_id": "555c0bf0d15bc0fc642aeffd",
    "content": "test123ffff"
},...
 ]
 */
module.exports.readAll = function(collection){
	var d = q.defer()
	var db = mongojs(db_name,[collection]);
		db[collection].find(function(err, docs){
			if(err) d.reject(err)
			else d.resolve(docs)		
		});
	return d.promise
}
